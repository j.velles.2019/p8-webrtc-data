import asyncio
import time
import json
import time
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

async def start_signaling(message):
    while UDPSignaling.noACK:
        print('Send:', message)
        UDPSignaling.transport.sendto(message.encode())
        await asyncio.sleep(2)


async def consume_signaling(pc, data):
    try:
        obj = RTCSessionDescription(**json.loads(data))
    except json.JSONDecodeError:
        if data == "ACK":
            UDPSignaling.noACK = False
            print("Waiting for a server")
            return
        if data == "GO AHEAD":
            print("Server found")
            jsonMessage = json.dumps({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})
            UDPSignaling.transport.sendto(jsonMessage.encode())
            return
        elif data is BYE:
            print("Exiting")
            return

    if isinstance(obj, RTCSessionDescription):
        await pc.setRemoteDescription(obj)


class UDPSignaling:
    noACK = True
    transport = None
    address = ("127.0.0.1", 9999)
    def __init__(self, message, on_con_lost, pc):
        self.pc = pc
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        UDPSignaling.transport = transport
        asyncio.ensure_future(start_signaling(self.message))

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        asyncio.ensure_future(consume_signaling(self.pc, data.decode()))



    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def connect(pc):

    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = "REGISTER CLIENT"                                                
    transport, protocol = await loop.create_datagram_endpoint(                  
        lambda: UDPSignaling(message, on_con_lost, pc),
        remote_addr=UDPSignaling.address)

    try:
        await on_con_lost
    finally:
        transport.close()

time_start = None
def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc, signaling):

    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    await pc.setLocalDescription(await pc.createOffer())
    await signaling

if __name__ == "__main__":
    pc = RTCPeerConnection()
    signaling = connect(pc)
    coro = run_offer(pc, signaling)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        UDPSignaling.transport.close()
        print("Close the socket")
