import asyncio
import json
import time

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription


async def start_signaling(message):
    while UDPSignaling.noACK:
        print('Send:', message)
        UDPSignaling.transport.sendto(message.encode())
        await asyncio.sleep(2)

async def consume_signaling(data):
    global pc
    try:
        jsonMessage = json.loads(data)
    except json.JSONDecodeError:
        if data == "ACK":
            UDPSignaling.noACK = False
            print("ACK received")
            return

    if jsonMessage["type"] == "bye":
        await pc.close()
        pc = RTCPeerConnection()
        print("Connection Finnished")
        return
    else:
        obj = RTCSessionDescription(**jsonMessage)
        if isinstance(obj, RTCSessionDescription):
            await open_connection(pc)
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                await pc.setLocalDescription(await pc.createAnswer())
                UDPSignaling.transport.sendto(json.dumps
                                              ({"sdp": pc.localDescription.sdp,
                                                "type": pc.localDescription.type}).encode())

async def open_connection(pc):

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)



time_start = None


class UDPSignaling:
    noACK = True
    transport = None
    address = ("127.0.0.1", 9999)
    def __init__(self, message, on_con_lost, pc):
        self.pc = pc
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        UDPSignaling.transport = transport
        asyncio.ensure_future(start_signaling(self.message))


    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        asyncio.ensure_future(consume_signaling(data.decode()))


    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def connect(pc):

    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = "REGISTER SERVER"                                               

    transport, protocol = await loop.create_datagram_endpoint(                 
        lambda: UDPSignaling(message, on_con_lost, pc),
        remote_addr=UDPSignaling.address)

    try:
        await on_con_lost
    finally:
        transport.close()

async def run_answer(pc, signaling):
    await signaling

if __name__ == "__main__":
    pc = RTCPeerConnection()
    signaling = connect(pc)
    coro = run_answer(pc, signaling)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
