import asyncio
import json



def finder(addr):
    for name, addrs in SignallingProtocol.members.items():
        if addrs["serverAddress"] == addr:
            return addrs["clientAddress"]
        if addrs["clientAddress"] == addr:
            return addrs["serverAddress"]


async def reception(data, addr, transport):
    message = data.decode()
    print('Received %r from %s' % (message, addr))

    if message[0:16] == 'REGISTER CLIENT ':
        while True:
            if SignallingProtocol.members[message[15:len(message)]]:
                SignallingProtocol.members[message[15:len(message)]]["clientAddress"] = addr
                break
            await asyncio.sleep(0.1)

        print(f"Client registered at {addr}")
        transport.sendto("ACK".encode(), SignallingProtocol.members[message[15:len(message)]]["clientAddress"])
        if SignallingProtocol.the_server_is_up:
            transport.sendto("GO AHEAD".encode(), SignallingProtocol.members[message[15:len(message)]]["clientAddress"])



    elif message[0:16] == 'REGISTER SERVER ':
        SignallingProtocol.members[message[15:len(message)]] = {"clientAddress": None, "serverAddress": addr}
        print(f"Server registered at {addr}")
        transport.sendto("ACK".encode(), SignallingProtocol.members[message[15:len(message)]]["serverAddress"])
        if SignallingProtocol.members[message[15:len(message)]]["clientAddress"] is None:
            while SignallingProtocol.members[message[15:len(message)]]["clientAddress"] is None:
                await asyncio.sleep(0.1)
        transport.sendto("GO AHEAD".encode(), SignallingProtocol.members[message[15:len(message)]]["clientAddress"])
        SignallingProtocol.the_server_is_up = True


    else:

        try:
            jsonMessage = json.loads(message)
            print("JSON received")
        except ValueError:
            print("Invalid JSON")
            return
        kind = jsonMessage['type']
        if kind == 'answer':
            print('Send %r to %s' % (kind, finder(addr)))
            transport.sendto(data, finder(addr))

        elif kind == 'offer':
            print('Send %r to %s' % (kind, finder(addr)))
            transport.sendto(data, finder(addr))



class SignallingProtocol:
    members = {}
    the_server_is_up = False
    def __init__(self):
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        asyncio.ensure_future(reception(data, addr, self.transport))







async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
